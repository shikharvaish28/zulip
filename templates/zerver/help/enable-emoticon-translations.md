# Enable emoticon translation

If you use emoticons like `:)` or `:/`, you can have them translated into
emoji equivalents like
<img
    src="/static/generated/emoji/images-google-64/1f642.png"
    alt="slight_smile"
    style="width: 3%;"
/>
or
<img
    src="/static/generated/emoji/images-google-64/1f641.png"
    alt="slight_frown"
    style="width: 3%;"
/>
automatically by Zulip.

{settings_tab|display-settings}

2. Select the option labeled
   **Convert emoticons before sending**.

## Current emoticon conversions

{emoticon_translations}
